<?php

namespace Tah\SuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

	public function indexAction() {
		return $this->render('TahSuBundle:Default:index.html.twig');
	}

	public function searchAction($keywords)
	{
		$url = "http://svcs.ebay.com/services/search/FindingService/v1?";
		$url .= http_build_query(
			array(
				'OPERATION-NAME' => 'findItemsByKeywords',
				'SERVICE-VERSION' => '1.0.0',
				'SECURITY-APPNAME' => $this->container->getParameter('ebay_appid'),
				'RESPONSE-DATA-FORMAT' => 'XML',
				'REST-PAYLOAD' => '1',
				'GLOBAL-ID' => 'EBAY-GB',
				'keywords' => $keywords . ' umbrella'
				)
			);

		// Send request to eBay and load response in $response
		$connection = curl_init();
		curl_setopt($connection, CURLOPT_URL, $url);
		curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($connection);
		curl_close($connection);

		$searchResults = array();

		//If it is false, searchResults is empty and index.html.twig will show an appropriate message
		if($response !== false) {
			$items = new \SimpleXMLElement($response);
			foreach($items->searchResult->item as $item) {
				$searchResults[] = (array)$item;
			}
		}

		return $this->render(
			'TahSuBundle:Default:results.html.twig',
			array(
				'name' => $keywords,
				'searchResults' => $searchResults
				)
			);
	}
}
